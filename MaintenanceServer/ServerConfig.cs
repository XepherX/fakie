﻿using Newtonsoft.Json;

namespace MaintenanceServer
{
    public class ServerConfig
    {
        [JsonProperty("connection")]
        public ConnectionProperties Connection { get; set; } = new ConnectionProperties();

        [JsonProperty("protocolName")]
        public string ProtocolName { get; set; } = "maintenance";

        [JsonProperty("serverDescription")]
        public string ServerDescription { get; set; } = "server offline for scheduled maintenance";

        [JsonProperty("kickMessage")]
        public string KickMessage { get; set; } = "Hey {player},\n\nThe server is currently unavailable for scheduled maintenance.\nSorry for the inconvenience!\n\n--admin";

    }

    public class ConnectionProperties
    {
        [JsonProperty("ipAddress")]
        public string IpAddress { get; set; } = "127.0.0.1";
        [JsonProperty("port")]
        public int Port { get; set; } = 25565;


    }
}
