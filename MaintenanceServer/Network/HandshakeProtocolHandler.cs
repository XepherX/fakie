﻿using MaintenanceServer.Network.Packets;
using MaintenanceServer.Network.Types;
using MaintenanceServer.Network.Types.JSON;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace MaintenanceServer.Network
{
    public class HandshakeProtocolHandler
    {

        public HandshakeProtocolHandler(ServerConfig serverConfig, string serverIcon)
        {
            ServerConfig = serverConfig;
            ServerIcon = serverIcon;
        }

        public ServerConfig ServerConfig { get; }
        public string ServerIcon { get; }

        public void HandleRequest(TcpClient client, Stream clientStream)
        {
            var handshake = HandshakePacket.FromStream(clientStream);

            switch (handshake.NextState.IntValue)
            {
                case 1:
                    {
                        var request = StatusRequestPacket.FromStream(clientStream);
                        var statusJson = new StatusJson
                        {
                            Description = new TextJson
                            {
                                Text = ServerConfig.ServerDescription
                            },
                            Version = new VersionJson
                            {
                                Name = ServerConfig.ProtocolName,
                                Protocol = 736
                            },
                            Players = new PlayersJson
                            {
                                Max = 0,
                                Online = 0
                            },
                            Favicon = "data:image/png;base64," + ServerIcon

                        };
                        var status = new StatusResponsePacket(statusJson);
                        clientStream.Write(status.ToBytes());
                        var pingPacket = PingRequestPacket.FromStream(clientStream);
                        clientStream.Write(pingPacket.ToBytes());

                        Console.WriteLine($"[{client.Client.RemoteEndPoint}] Handshake finished, status sent.");
                        clientStream.Close();

                        break;
                    }
                case 2:
                    {

                        var loginStart = LoginStartPacket.FromStream(clientStream);
                        var kickMessage = ServerConfig.KickMessage.Replace("{player}", loginStart.PlayerName.Data);
                        var kick = new BasePacket
                        {
                            PacketId = new VarInt(0),
                            Data = new List<IToBytes>
                                            {
                                                new NetString($"{{\"text\": \"{kickMessage}\"}}")
                                            }
                        };

                        clientStream.Write(kick.ToBytes());
                        Console.WriteLine($"[{client.Client.RemoteEndPoint}] Connection gracefully dropped for {loginStart.PlayerName.Data}");
                        clientStream.Close();

                        break;
                    }
            }
        }
    }
}
