﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MaintenanceServer.Network
{
    public static class EzReader
    {
        public static byte[] Read(this Stream stream)
        {
            var messageBuffer = new List<byte>();
            var readBuffer = new byte[4096];
            int readBytes;
            do
            {
                readBytes = stream.Read(readBuffer, 0, readBuffer.Length);
                messageBuffer.AddRange(readBuffer.Take(readBytes));
            } while (readBytes == readBuffer.Length);

            return messageBuffer.ToArray();
        }
    }
}
