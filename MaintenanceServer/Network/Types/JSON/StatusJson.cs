﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MaintenanceServer.Network.Types.JSON
{
    public class StatusJson
    {
        [JsonProperty("version")]
        public VersionJson Version { get; set; }
        [JsonProperty("players")]
        public PlayersJson Players { get; set; }
        [JsonProperty("description")]
        public TextJson Description { get; set; }
        [JsonProperty("favicon")]
        public string Favicon { get; set; }
    }

    public class VersionJson
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("protocol")]
        public int Protocol { get; set; }
    }
    public class SampleJson
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class PlayersJson
    {
        [JsonProperty("max")]
        public int Max { get; set; }
        [JsonProperty("online")]
        public int Online { get; set; }
        [JsonProperty("sample")]
        public List<SampleJson> Sample { get; set; }
    }

    public class TextJson
    {
        [JsonProperty("text")]
        public string Text { get; set; }
    }

}
