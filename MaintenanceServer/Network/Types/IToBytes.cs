﻿namespace MaintenanceServer.Network.Types
{
    public interface IToBytes
    {
        byte[] ToBytes();
    }
}
