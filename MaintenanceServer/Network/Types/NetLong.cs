﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace MaintenanceServer.Network.Types
{
    public class NetLong : IToBytes
    {

        private NetLong(long value, byte[] data)
        {
            Value = value;
            Data = data;
        }

        public NetLong(long value)
        {
            Value = value;

            var buf = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
                buf = buf.Reverse().ToArray();
            Data = buf;
        }

        public byte[] Data { get; set; }

        public long Value { get; set; }

        public byte[] ToBytes()
        {
            return Data;
        }

        public static NetLong FromStream(Stream stream)
        {
            var buf = new byte[8];
            stream.Read(buf, 0, buf.Length);

            if (BitConverter.IsLittleEndian)
                buf = buf.Reverse().ToArray();

            return new NetLong(BitConverter.ToInt64(buf), buf);
        }
    }
}
