﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MaintenanceServer.Network.Types
{
    public class VarInt : IToBytes
    {
        public int IntValue { get; private set; }
        public byte[] BytesValue { get; private set; }

        public VarInt(IEnumerable<byte> bytes)
        {
            int numRead = 0;
            int result = 0;
            byte read;
            var bytearray = bytes.ToArray();
            do
            {
                read = bytearray[numRead];
                int value = read & 0b01111111;
                result |= value << (7 * numRead);

                numRead++;
                if (numRead > 5)
                {
                    throw new ArgumentException("VarInt is too big");
                }
            } while ((read & 0b10000000) != 0);

            IntValue = result;
            BytesValue = bytearray;
        }

        public VarInt(int integer)
        {
            var value = (ulong)integer;
            var buffer = new byte[10];
            var pos = 0;
            do
            {
                var byteVal = value & 0x7f;
                value >>= 7;

                if (value != 0)
                {
                    byteVal |= 0x80;
                }

                buffer[pos++] = (byte)byteVal;

            } while (value != 0);

            var result = new byte[pos];
            Buffer.BlockCopy(buffer, 0, result, 0, pos);

            IntValue = integer;
            BytesValue = result;
        }

        private VarInt(int intValue, byte[] byteValue)
        {
            IntValue = intValue;
            BytesValue = byteValue;
        }

        public byte[] ToBytes()
        {
            return BytesValue;
        }

        public static VarInt FromStream(Stream stream)
        {
            int numRead = 0;
            int result = 0;
            byte read;
            List<byte> allRead = new List<byte>();
            do
            {
                read = (byte)stream.ReadByte();
                allRead.Add(read);
                int value = read & 0b01111111;
                result |= value << (7 * numRead);

                numRead++;
                if (numRead > 5)
                {
                    throw new ArgumentException("VarInt is too big");
                }
            } while ((read & 0b10000000) != 0);

            return new VarInt(result, allRead.ToArray());
        }
    }
}
