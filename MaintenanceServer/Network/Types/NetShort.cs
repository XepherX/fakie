﻿using System;
using System.IO;
using System.Linq;

namespace MaintenanceServer.Network.Types
{
    public class NetShort : IToBytes
    {
        public short Value { get; private set; }
        private byte[] Data { get; set; }
        public NetShort(short value)
        {
            Value = value;
            var buf = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
                buf = buf.Reverse().ToArray();
            Data = buf;

        }

        private NetShort(short value, byte[] data)
        {
            Value = value;
            Data = data;
        }

        public byte[] ToBytes()
        {
            return Data;
        }

        public static NetShort FromStream(Stream stream)
        {
            var buf = new byte[2];
            stream.Read(buf, 0, buf.Length);

            if (BitConverter.IsLittleEndian)
                buf = buf.Reverse().ToArray();

            return new NetShort(BitConverter.ToInt16(buf), buf);
        }
    }
}
