﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MaintenanceServer.Network.Types
{
    public class NetString : IToBytes
    {
        public VarInt Length => new VarInt(DataBytes.Length);
        public string Data { get; private set; }
        private byte[] DataBytes { get; set; }

        public NetString(string data)
        {
            Data = data;
            DataBytes = Encoding.UTF8.GetBytes(data);
        }

        private NetString(string data, byte[] dataBytes)
        {
            Data = data;
            DataBytes = dataBytes;
        }


        public byte[] ToBytes()
        {
            var result = new List<byte>();
            result.AddRange(Length.BytesValue);
            result.AddRange(DataBytes);
            return result.ToArray();
        }

        public static NetString FromStream(Stream stream)
        {
            var length = VarInt.FromStream(stream);
            var buf = new byte[length.IntValue];
            stream.Read(buf, 0, buf.Length);

            return new NetString(Encoding.UTF8.GetString(buf), buf);
        }
    }
}
