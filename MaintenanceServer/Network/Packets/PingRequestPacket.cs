﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MaintenanceServer.Network.Types;

namespace MaintenanceServer.Network.Packets
{
    public class PingRequestPacket : BasePacket
    {
        public PingRequestPacket()
        {
            PacketId = new VarInt(1);
        }

        public NetLong Payload { get; set; }

        public override List<IToBytes> Data => new List<IToBytes> { Payload };

        public static PingRequestPacket FromStream(Stream stream)
        {
            var packet = new PingRequestPacket();
            VarInt.FromStream(stream);
            packet.PacketId = VarInt.FromStream(stream);
            packet.Payload = NetLong.FromStream(stream);
            return packet;
        }
    }
}
