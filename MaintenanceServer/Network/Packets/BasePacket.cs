﻿using System.Collections.Generic;
using System.Linq;
using MaintenanceServer.Network.Types;

namespace MaintenanceServer.Network.Packets
{
    public class BasePacket : IToBytes
    {
        public VarInt Length => new VarInt(PacketId.BytesValue.Length + Data.Sum(q => q.ToBytes().Length));
        public VarInt PacketId { get; set; }
        public virtual List<IToBytes> Data { get; set; } = new List<IToBytes>();
        public virtual byte[] ToBytes()
        {
            var result = new List<byte>();
            result.AddRange(Length.BytesValue);
            result.AddRange(PacketId.BytesValue);
            result.AddRange(Data.SelectMany(q => q.ToBytes()));
            return result.ToArray();
        }
    }
}
