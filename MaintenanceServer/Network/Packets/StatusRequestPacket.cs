﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MaintenanceServer.Network.Types;

namespace MaintenanceServer.Network.Packets
{
    public class StatusRequestPacket : BasePacket
    {
        public StatusRequestPacket()
        {
            PacketId = new VarInt(0);
        }


        public static StatusRequestPacket FromStream(Stream stream)
        {
            VarInt.FromStream(stream); //read and discard length;
                var packet = new StatusRequestPacket
            {
                PacketId = VarInt.FromStream(stream)
            };
            return packet;
        }
    }
}
