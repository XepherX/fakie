﻿using System.Collections.Generic;
using System.IO;
using MaintenanceServer.Network.Types;

namespace MaintenanceServer.Network.Packets
{
    public class HandshakePacket : BasePacket
    {
        public HandshakePacket()
        {
            this.PacketId = new VarInt(0);
        }

        public VarInt ProtocolVersion { get; set; }
        public NetString ServerAddress { get; set; }
        public NetShort ServerPort { get; set; }
        public VarInt NextState { get; set; }

        public override List<IToBytes> Data => new List<IToBytes> { ProtocolVersion, ServerAddress, ServerPort, NextState };

        public static HandshakePacket FromStream(Stream stream)
        {
            var packet = new HandshakePacket();
            VarInt.FromStream(stream); // read length of packet from stream, then discard it.
            packet.PacketId = VarInt.FromStream(stream);
            packet.ProtocolVersion = VarInt.FromStream(stream);
            packet.ServerAddress = NetString.FromStream(stream);
            packet.ServerPort = NetShort.FromStream(stream);
            packet.NextState = VarInt.FromStream(stream);
            return packet;
        }
    }
}
