﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MaintenanceServer.Network.Types;

namespace MaintenanceServer.Network.Packets
{
    public class LoginStartPacket : BasePacket
    {
        public LoginStartPacket()
        {
            PacketId = new VarInt(0);
        }

        public NetString PlayerName { get; set; }

        public static LoginStartPacket FromStream(Stream stream)
        {
            var packet = new LoginStartPacket();
            VarInt.FromStream(stream);
            packet.PacketId = VarInt.FromStream(stream);
            packet.PlayerName = NetString.FromStream(stream);
            return packet;
        }
    }
}
