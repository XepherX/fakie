﻿using MaintenanceServer.Network.Types;
using MaintenanceServer.Network.Types.JSON;
using Newtonsoft.Json;

namespace MaintenanceServer.Network.Packets
{
    public class StatusResponsePacket : BasePacket
    {
        public StatusResponsePacket(StatusJson status)
        {
            PacketId = new VarInt(0);
            base.Data.Add(new NetString(JsonConvert.SerializeObject(status)));
        }
    }
}
