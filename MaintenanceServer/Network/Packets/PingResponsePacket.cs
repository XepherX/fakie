﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MaintenanceServer.Network.Types;

namespace MaintenanceServer.Network.Packets
{
    public class PingResponsePacket : BasePacket
    {
        public PingResponsePacket(NetLong payload)
        {
            PacketId = new VarInt(1);
            Payload = payload;
        }

        private PingResponsePacket()
        {

        }

        public NetLong Payload { get; set; }

        public override List<IToBytes> Data => new List<IToBytes> { Payload };

        public static PingResponsePacket FromStream(Stream stream)
        {
            var packet = new PingResponsePacket();
            VarInt.FromStream(stream); // read length and discard
            packet.PacketId = VarInt.FromStream(stream);
            packet.Payload = NetLong.FromStream(stream);
            return packet;
        }
    }
}
