﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;
using MaintenanceServer.Network;
using MaintenanceServer.Network.Packets;
using MaintenanceServer.Network.Types;
using MaintenanceServer.Network.Types.JSON;
using Newtonsoft.Json;

namespace MaintenanceServer
{
    class Program
    {
        public static readonly string ConfigFileName = "./server-config.json";
        public static readonly string IconFileName = "./server-icon.png";
        static void Main(string[] args)
        {
            ServerConfig config;
            if (!File.Exists(Program.ConfigFileName))
            {
                config = new ServerConfig();
                File.WriteAllText(Program.ConfigFileName, JsonConvert.SerializeObject(config, Formatting.Indented));
            }
            else
            {
                config = JsonConvert.DeserializeObject<ServerConfig>(File.ReadAllText(Program.ConfigFileName));
            }

            string iconB64;
            if (!File.Exists(Program.IconFileName))
            {
                File.WriteAllBytes(Program.IconFileName, Resources._default);
                iconB64 = Convert.ToBase64String(Resources._default);
            }
            else
            {
                iconB64 = Convert.ToBase64String(File.ReadAllBytes(Program.IconFileName)); // todo: image resizing, or any error handling lol
            }

            var tcpServer = new TcpListener(IPAddress.Parse(config.Connection.IpAddress), config.Connection.Port);
            tcpServer.Start();

            Console.WriteLine($"Server listening on {config.Connection.IpAddress}:{config.Connection.Port}");


            var protocolHandler = new HandshakeProtocolHandler(config, iconB64);

            while (true)
            {
                var client = tcpServer.AcceptTcpClient();

                Task.Run(() =>
                {
                    Console.WriteLine($"[{client.Client.RemoteEndPoint}] Connected");
                    using (var clientStream = client.GetStream())
                    {
                        try
                        {
                            while (clientStream.CanRead)
                            {
                                protocolHandler.HandleRequest(client, clientStream);
                            }
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine($"[ERROR]: fatal exception occurred");
                            Console.WriteLine(exception.StackTrace);
                        }
                        finally
                        {
                            client.Dispose();
                        }

                    }
                });



            }
        }
    }
}
